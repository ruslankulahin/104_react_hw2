import { useEffect, useState } from "react";
import PropTypes from "prop-types";
import ProductList from "./components/ProductList";
import Modal from "./components/Modal";
import Button from "./components/Button";
import { sendRequest } from "./helpers/sendRequest";
import { faCartShopping } from "@fortawesome/free-solid-svg-icons";
import { faStar } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "./App.scss";

export default function App() {
    const [products, setProducts] = useState([]);
    const [cartItems, setCartItems] = useState(
        JSON.parse(localStorage.getItem("cartItems")) || []
    );
    const [favoriteItems, setFavoriteItems] = useState(
        JSON.parse(localStorage.getItem("favoriteItems")) || []
    );
    const [showCartModal, setShowCartModal] = useState(false);
    const [showFavoritesModal, setShowFavoritesModal] = useState(false);

    useEffect(() => {
        sendRequest("products.json").then((products) => {
            setProducts(products);
        });
    }, []);
    // console.log({cartItems});

    const handleAddToCart = (product) => {
        const indexItem = cartItems.findIndex((item) => item.id === product.id);
        console.log(cartItems);
        if (indexItem === -1) {
            const newItem = {
                id: product.id,
                name: product.name,
                price: product.price,
                image: product.image,
                quantity: 1,
            };
            const mergedCartItems = [...cartItems, newItem];
            setCartItems(mergedCartItems);
            localStorage.setItem("cartItems", JSON.stringify(mergedCartItems));
            // console.log(mergedCartItems);
        } else {
            const updatedItem = {
                ...cartItems[indexItem],
                quantity: cartItems[indexItem].quantity + 1,
            };
            const updatedCartItems = [...cartItems];
            updatedCartItems.splice(indexItem, 1, updatedItem);

            setCartItems(updatedCartItems);
            localStorage.setItem("cartItems", JSON.stringify(updatedCartItems));
            // console.log(updatedCartItems);
        }
    };

    const handleRemoveFromCart = (product) => {
        const updatedCartItems = [...cartItems];

        const indexItem = updatedCartItems.findIndex(
            (item) => item.id === product.id
        );

        if (indexItem !== -1) {
            updatedCartItems[indexItem].quantity -= 1;

            if (updatedCartItems[indexItem].quantity === 0) {
                updatedCartItems.splice(indexItem, 1);
            }

            setCartItems(updatedCartItems);
            localStorage.setItem("cartItems", JSON.stringify(updatedCartItems));
            // console.log(updatedCartItems);
        }
    };

    const handleToggleCartModal = () => {
        setShowCartModal(!showCartModal);
    };

    const handleAddToFavorites = (product) => {
        const indexItem = favoriteItems.findIndex(
            (item) => item.id === product.id
        );

        if (indexItem === -1) {
            const newItem = {
                id: product.id,
                name: product.name,
                price: product.price,
                image: product.image,
            };
            const mergedFavoriteItems = [...favoriteItems, newItem];

            setFavoriteItems(mergedFavoriteItems);
            localStorage.setItem(
                "favoriteItems",
                JSON.stringify(mergedFavoriteItems)
            );
            console.log(mergedFavoriteItems);
        } else {
            const updatedFavoriteItems = favoriteItems.filter(
                (item) => item.id !== product.id
            );
            setFavoriteItems(updatedFavoriteItems);
            localStorage.setItem(
                "favoriteItems",
                JSON.stringify(updatedFavoriteItems)
            );
            console.log(updatedFavoriteItems);
        }
    };

    const handleToggleFavoritesModal = () => {
        setShowFavoritesModal(!showFavoritesModal);
    };

    const handleOutsideCartClick = (event) => {
        if (!event.target.closest(".modal")) {
            handleToggleCartModal();
        }
    };

    const handleOutsideFavoritClick = (event) => {
        if (!event.target.closest(".modal")) {
            handleToggleFavoritesModal();
        }
    };

    const handleItemCartClose = (id) => {
        const updatedCartItems = cartItems.filter((item) => item.id !== id);

        setCartItems(updatedCartItems);
        localStorage.setItem("cartItems", JSON.stringify(updatedCartItems));
        // console.log(updatedCartItems);
    };

    const cartItemTotal = cartItems.reduce(
        (total, item) => total + item.quantity,
        0
    );
    const totalPrice = cartItems.reduce(
        (total, item) => total + item.price * item.quantity,
        0
    );
    const favoriteItemTotal = favoriteItems.length;

    return (
        <div className="app-wrapper">
            <header className="header">
                <div className="container header-wrapper">
                    <h1 className="title">
                        <span> THE BEST</span>
                        <br></br> products <span>ONLY FOR YOU</span>
                    </h1>
                    <div className="icons-wrapper">
                        <div
                            className="cart-shopping-wrapper"
                            onClick={() => handleToggleCartModal()}
                        >
                            <FontAwesomeIcon
                                icon={faCartShopping}
                                size="sm"
                                style={{
                                    color: cartItems.length ? "#f4f88b" : "blue",
                                }}
                            />{" "}
                            <span>{cartItemTotal}</span>
                        </div>
                        <div
                            className="star-wrapper"
                            onClick={() => handleToggleFavoritesModal()}
                        >
                            <FontAwesomeIcon
                                icon={faStar}
                                size="sm"
                                style={{
                                    outlineColor: "blue",
                                    color: favoriteItems.length
                                        ? "#f4f88b"
                                        : "blue",
                                }}
                            />{" "}
                            <span>{favoriteItemTotal}</span>
                        </div>
                    </div>
                </div>
            </header>
            <ProductList
                products={products}
                cartItems={cartItems}
                favoriteItems={favoriteItems}
                AddToCart={handleAddToCart}
                RemoveFromCart={handleRemoveFromCart}
                AddToFavorites={handleAddToFavorites}
                showCartModal={handleToggleCartModal}
                ItemCartClose={handleItemCartClose}
            />
            {showCartModal && (
                <Modal
                    classNames="shoping-cart__modal"
                    cartItems={cartItems}
                    closeButton={handleToggleCartModal}
                    closeModal={handleToggleCartModal}
                    handleOutsideClick={handleOutsideCartClick}
                    header="Кошик"
                    text={!cartItems.length && <div>Ваш кошик порожній.</div>}
                    actions={cartItems.map((item) => (
                        <div className="item-wrapper__modal" key={item.id}>
                            <div className="item-text__modal">
                                <div className="item-name__modal">
                                    {item.name}
                                </div>
                                <div className="item-price__modal">
                                    {item.price} грн
                                </div>
                            </div>
                            <div className="add-remove-btn__wrapper">
                                <Button
                                    className="btn minus-from-cart"
                                    onClick={() => handleRemoveFromCart(item)}
                                >
                                    -
                                </Button>
                                <div className="quantity-product">
                                    {item.quantity}
                                </div>
                                <Button
                                    className="btn plus-to-cart"
                                    onClick={() => handleAddToCart(item)}
                                >
                                    +
                                </Button>
                                <Button
                                    className="btn close-item__modal"
                                    onClick={() => handleItemCartClose(item.id)}
                                >
                                    &times;
                                </Button>
                            </div>
                        </div>
                    ))}
                    totalPrice={
                        cartItems.length && (
                            <div className="total-price__modal">
                                Разом: {totalPrice} грн
                            </div>
                        )
                    }
                />
            )}
            {showFavoritesModal && (
                <Modal
                    classNames="favorites__modal"
                    favoriteItems={favoriteItems}
                    closeButton={handleToggleFavoritesModal}
                    closeModal={handleToggleFavoritesModal}
                    handleOutsideClick={handleOutsideFavoritClick}
                    header="Улюблені товари"
                    text={
                        !favoriteItems.length && (
                            <div>У вас ще немає улюблених товарів.</div>
                        )
                    }
                    actions={favoriteItems.map((item) => (
                        <div className="item-wrapper__modal" key={item.id}>
                            <div className="item-text__modal">
                                <div className="item-name__modal">
                                    {item.name}
                                </div>
                                <div className="item-price__modal">
                                    {item.price} грн
                                </div>
                            </div>
                            <Button
                                className="btn__favorite-modal"
                                onClick={() => {
                                    handleAddToCart(item);
                                }}
                            >
                                Додати до кошика
                            </Button>
                        </div>
                    ))}
                    // totalPrice=""
                />
            )}
        </div>
    );
}

App.propTypes = {
    products: PropTypes.array,
    cartItems: PropTypes.array,
    favoriteItems: PropTypes.array,
    AddToCart: PropTypes.func,
    RemoveFromCart: PropTypes.func,
    AddToFavorites: PropTypes.func,
    showCartModal: PropTypes.func,
    ItemCartClose: PropTypes.func,
    classNames: PropTypes.string,
    closeButton: PropTypes.func,
    closeModal: PropTypes.bool,
    handleOutsideClick: PropTypes.func,
    handleToggleFavoritesModal: PropTypes.func,
    handleAddToCart: PropTypes.func,
    header: PropTypes.object,
    text: PropTypes.any,
    totalPrice: PropTypes.number,
};
