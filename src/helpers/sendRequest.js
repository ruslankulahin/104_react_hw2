export const sendRequest = async (url) => {
	const response = await fetch(url);
	const products = await response.json();
	return products;
}